# RCDiskSwitcher

## Summary

Arduino based project to switch power between 8 external DC 12V hard disks.

Currently a maximum of 2 disks can be powered simultaneously.

## Hardware

- 1 x Arduino Micro
- 1 x IR receiver
- 1 x 8 Channel 5V Relay board
- 1 x 35V to 5V Switching Regulator
- 1 x DC 12V 4 Amp power supply
- 8 x Panel mount LEDs
- 9 x DC Sockets

## Build & Deploy

1. Open the box (4 Screws)
2. Unplug micro USB from the Arduino
3. Connect a USB cable from your computer to the Arduino micro USB port
4. Download Arduino IDE - https://www.arduino.cc/en/Main/Software
5. Delete libraries/RobotIRremote/ - This library conflicts with the IR library we're actually using
6. Sketch Menu -> Include Library -> Add .zip library... -> Choose TomsRemoteDisks/libraries/Arduino-IRremote-master.zip
7. Tools Menu -> Port -> Select Arduino Port
7. Click: Upload

## Potential Customizations

### Allow more than disks to be powered concurrently

The current limit of 2 is fairly cautious, and could be increased depending on the power usage by the disks.  The maximum can be easily calculated:
Number of concurrently powered disks = <Ouput AMPs of power supply (Currently 4 Amps)> / <Imput Amps of hard disks>

1. Edit the ino file
2. Set the MAX_POWERED_OUTPUTS in the code
3. Deploy code to Arduino (See Build & Deploy above)

###  Pair with a different remote control

The Arduino and IR receiver can handle most remote controls.  With a bit of effort, it would be possible switch disks using any remote control.

1. Connect the Arduino to your computer (See Build & Deploy above)
2. View console
3. Get any remote control, point it at the box, press a button
4. Get hex code printed out on console (Console line should show: "=== IR Button pressed: AB3233")
5. Set this hex code in the irHexToNum method in the ino file
6. Repeat for all buttons
7. Deploy code to Arduino
