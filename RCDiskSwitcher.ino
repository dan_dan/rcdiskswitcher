
#include "EEPROM.h"

#include <IRremote.h>


/***
 * Tom's Remote Disks
 * ==================
 *
 * Arduino program to switch power between a set of 8 external hard disks.
 *
 * Author: Daniel@laeng.org
 * Source Code: https://bitbucket.org/dan_dan/rcdiskswitcher
 *
 * Terminology
 * -----------
 *
 * Remote control has a bunch of "buttons". - 0=off, 1=disk1, 2=disk2, etc
 * Box has 8 "outputs", 0=disk1, 1=disk2, 2=disk3, etc
 * We are using 8 pins to power the 8 disks.  pin2=disk1, pin3=disk2, pin4=disk3
 *
 * OutputNum is used to map between buttons and disks.
 *
 * Output: 0
 * Button: 1
 * Disk: 1
 * Pin: 2
 *
 * Output: 1
 * Button: 2
 * Disk: 2
 * Pin: 3
 *
 * And so on.
 *
 ***/
int IR_RECV_PIN = 11; // Pin IR Reciever is connected to
const int NUM_OUTPUTS = 8; // Number of outputs pins
const int OUTPUT_PINS[NUM_OUTPUTS] = { 2, 3, 4, 5, 6, 7, 8, 9 }; // Output Pin numbers, map to Disks 0, 1, 2, 3, 4, ...
int PIN_STATES[NUM_OUTPUTS] = { 0, 0, 0, 0, 0, 0, 0, 0}; // State of the pins
const int MAX_POWERED_OUTPUTS = 2;
const int TRUE = 1;
const int FALSE = 0;
const int NONE = 255;

const int DISK_ON = LOW;  // Use inverse power for relay
const int DISK_OFF = HIGH;

IRrecv irrecv(IR_RECV_PIN);
decode_results results;

/**
 * Setup - called on power up
 **/
void setup()
{
  Serial.begin(9600);
  Serial.println("=== Starting Setup ===");
  irrecv.enableIRIn(); // Start the receiver

  restoreStateFromEeprom();
  for(int i = 0; i < NUM_OUTPUTS; i++) {
    pinMode(OUTPUT_PINS[i], OUTPUT);
  }

  Serial.println("=== Setup Complete ===");
}


/**
 * Main loop
 * Called automagically by framework
 **/
void loop() {

  if (irrecv.decode(&results)) {
    Serial.println("=== IR Button pressed: " + String(results.value, HEX) + "===");

    int buttonNum = irHexToNum(results.value);
    Serial.println("IR Hex converted to button num: " + String(buttonNum));

    if (buttonNum == 0) {
      switchAllOff(TRUE);
    }
    else if (buttonNum >= 1 && buttonNum <= 8) {
      toggleOutput(buttonNum - 1);
    }

    irrecv.resume(); // Receive the next value
  }
  delay(100);
}

/**
 * Set output to specified value (on/off)
 **/
void setOutput(int outputNum, int value) {
  setOutputPrivate(outputNum, value, TRUE);
}

/**
 * Set output to specified value (on/off)
 **/
void setOutputSkipSaveToEeprom(int outputNum, int value) {
  setOutputPrivate(outputNum, value, FALSE);
}

/**
 * Set output to specified value (on/off), with optional saving to Eeprom
 **/
void setOutputPrivate(int outputNum, int value, int saveToEeprom) {
  int pinNum = OUTPUT_PINS[outputNum];
  Serial.println("=== Setting output " + String(outputNum) + " (" + pinNum + ") to: " + value);

  if (value && countPoweredOutputs() >= MAX_POWERED_OUTPUTS) {
    Serial.println("No, do not power on, max powered outputs reached");
    return;
  }

  PIN_STATES[outputNum] = value;
  int on_off = value ? DISK_ON : DISK_OFF;
  digitalWrite(pinNum, on_off);

  if (saveToEeprom)
    saveStateToEeprom();
}

/**
 * Invert output
 **/
void toggleOutput(int outputNum) {
  setOutput(outputNum, !PIN_STATES[outputNum]);
}

/**
 * Turn all outputs off
 **/
void switchAllOff(int saveEeprom) {
  Serial.println("=== switchAllOff ===");
  for(int i = 0; i < NUM_OUTPUTS; i++) {
    setOutputPrivate(i, FALSE, saveEeprom);
  }
}

/**
 * Count the number of outputs currently switched on
 **/
int countPoweredOutputs() {
  int count = 0;
  for(int i = 0; i < NUM_OUTPUTS; i++) {
    if (PIN_STATES[i]) {
      count++;
    }
  }
  return count;
}

/**
 * Load state from Eeprom, and set outputs
 **/
void restoreStateFromEeprom() {
  Serial.println("=== restoreStateFromEeprom ===");
  switchAllOff(FALSE);

  // Load up to 2 powered outputs
  for(int pos = 0; pos < MAX_POWERED_OUTPUTS; pos++) {
    int outputNum = EEPROM.read(pos);

    Serial.println("Load [" + String(pos) + "] = " + String(outputNum));

    if (outputNum >= 0 && outputNum < NUM_OUTPUTS) {
      setOutputSkipSaveToEeprom(outputNum, TRUE);
    }
  }

}


/**
 * Save current state of outputs to Eeprom
 **/
void saveStateToEeprom() {
  Serial.println("=== saveStateToEeprom!! ===");
  int pos = 0;

  // Set up to 2 powered outputs
  for(int i = 0; (i < NUM_OUTPUTS && pos < MAX_POWERED_OUTPUTS); i++) {
    if (PIN_STATES[i]) {
      EEPROM.update(pos, i);
      Serial.println("Save [" + String(pos) + "] = " + String(i));
      pos++;
    }
  }

  // Set other outputs to off
  for(; pos < MAX_POWERED_OUTPUTS; pos++) {
    EEPROM.update(pos, NONE);
    Serial.println("Save [" + String(pos) + "] = " + String(NONE));
  }
}



/**
 * Convert hex infra-red to button number
 **/
int irHexToNum(long hex) {
   switch(hex) {
      case 0xFF6897 :
         return 0;
      case 0xFF30CF :
         return 1;
      case 0xFF18E7 :
         return 2;
      case 0xFF7A85 :
         return 3;
      case 0xFF10EF :
         return 4;
      case 0xFF38C7 :
         return 5;
      case 0xFF5AA5 :
         return 6;
      case 0xFF42BD :
         return 7;
      case 0xFF4AB5 :
         return 8;
      case 0xFF52AD :
         return 9;
      default:
         return -1;
   }

}
